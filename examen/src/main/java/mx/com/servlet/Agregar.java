package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.dao.AutomovilDAO;
import mx.com.dto.AutomovilDto;
import mx.com.service.AutomovilService;


/**
 * Servlet implementation class Agregar
 */
@WebServlet("/agregar")
public class Agregar extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private AutomovilService autoService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("null")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String modelo=request.getParameter("modelo");
		String marca=request.getParameter("marca");
		String placas = request.getParameter("placas");
		autoService.agregarAuto(modelo, marca, placas);
		PrintWriter out = null;
		out.println("<html>");
		out.println("<body>");
		out.println("<h1>Datos recibidos</h1");
		out.println(placas);
		out.println("</body>");
		out.println("</html>");
		
	}

}
