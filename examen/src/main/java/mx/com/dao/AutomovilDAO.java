package mx.com.dao;

import java.util.List;
import mx.com.bean.Automovil;

public interface AutomovilDAO {
	
	public List<Automovil> getAutos();
	
	public void agregarAutos(Automovil auto);

	static AutomovilDAO getInstance() {
		return null;
	}
}
