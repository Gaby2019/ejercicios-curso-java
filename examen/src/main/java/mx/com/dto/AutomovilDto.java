package mx.com.dto;

public class AutomovilDto {

	    private String modelo;
	    private String marca;
	    private String placas;
	    
	    /*
	    AutomovilDto(String modelo, String marca, String placas) {
	        this.modelo = modelo;
	        this.marca = marca;
	        this.placas = placas;
	      }
	    */
		public String getModelo() {
			return modelo;
		}
		public void setModelo(String modelo) {
			this.modelo = modelo;
		}
		public String getMarca() {
			return marca;
		}
		public void setMarca(String marca) {
			this.marca = marca;
		}
		public String getPlacas() {
			return placas;
		}
		public void setPlacas(String placas) {
			this.placas = placas;
		}
	    
	    
}
