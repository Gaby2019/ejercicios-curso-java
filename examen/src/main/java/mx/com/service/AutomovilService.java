package mx.com.service;

import java.util.List;
import mx.com.dto.AutomovilDto;
import mx.com.bean.Automovil;

public interface AutomovilService {
	List<AutomovilDto> getAuto();
	
	//void agregarAuto(AutomovilDto auto);
	void agregarAuto(String modelo, String marca, String placas);

	static AutomovilService getInstance() {
		return new AutomovilServiceImpl();
	}
}
