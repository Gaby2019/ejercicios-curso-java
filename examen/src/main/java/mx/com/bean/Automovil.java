package mx.com.bean;

public class Automovil {
	 	private String modelo;
	    private String marca;
	    private String placas;
	    private double precio;
	    
		public String getModelo() {
			return modelo;
		}
		public void setModelo(String modelo) {
			this.modelo = modelo;
		}
		public String getMarca() {
			return marca;
		}
		public void setMarca(String marca) {
			this.marca = marca;
		}
		public String getPlacas() {
			return placas;
		}
		public void setPlacas(String placas) {
			this.placas = placas;
		}
		public double getPrecio() {
			return precio;
		}
		public void setPrecio(double precio) {
			this.precio = precio;
		}
}
