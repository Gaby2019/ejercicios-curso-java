/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.praxis.daos.PersonDao;
import mx.com.praxis.entities.Person;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
@Stateless
public class PersonDaoImpl implements PersonDao {

	@PersistenceContext(unitName = "DBUnit")
	private  EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> findAll(final int page, final int size) {

		final Query query = entityManager.createNamedQuery("Person.findAll");
		query.setFirstResult((page - 1) * size);
		query.setMaxResults(size);

		final List<Person> results = query.getResultList();

		return results;
	}

	@Override
	public Person findById(final Long id) {
		final Query query = entityManager.createNamedQuery("Person.findById");
		query.setParameter("id", id);

		final Person result = (Person) query.getSingleResult();

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> findByFistName(final int page, final int size, final String firstName) {

		final Query query = entityManager.createNamedQuery("Person.findByFistName");
		query.setFirstResult((page - 1) * size);
		query.setMaxResults(size);
		query.setParameter("firstName", firstName);

		final List<Person> results = query.getResultList();

		return results;
	}

	@Override
	public void update(final Person entity) {

		final Person emPerson = entityManager.find(Person.class, entity.getId());

		emPerson.setLastName(entity.getLastName());
		emPerson.setFirstName(entity.getFirstName());
		entityManager.merge(emPerson);
	}

	@Override
	public void insert(final Person entity) {

		entityManager.persist(entity);
	}
}
