/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.daos.impl;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.praxis.daos.StudentDao;
import mx.com.praxis.entities.Student;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
@Stateless
public class StudentDaoImpl implements StudentDao {

	@PersistenceContext(unitName="DBUnit")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Student> findAll(final int page, final int size) {

		final Query query = entityManager.createNamedQuery("Student.findAll");
		query.setFirstResult((page - 1) * size);
		query.setMaxResults(size);

		final List<Student> results = query.getResultList();

		return results;
	}

	@Override
	public Student findById(final Long id) {

		final Query query = entityManager.createNamedQuery("Student.findById");
		query.setParameter("id", id);

		final Student result = (Student) query.getSingleResult();

		return result;
	}

	@Override
	public Student findByIdPerson(final long idPerson) {

		final Query query = entityManager.createNamedQuery("Student.findByIdPerson");
		query.setParameter("idPerson", idPerson);

		final Student result = (Student) query.getSingleResult();

		return result;
	}

	@Override
	public void update(final Student entity) {

		final Student emStudent = entityManager.find(Student.class, entity.getId());

		emStudent.setUniversityCareer(entity.getUniversityCareer());
		emStudent.setYearsBachelorDegree(entity.getYearsBachelorDegree());
		entityManager.merge(emStudent);
		
	}

	@Override
	public void insert(final Student entity) {

		entityManager.persist(entity);
	}

}
