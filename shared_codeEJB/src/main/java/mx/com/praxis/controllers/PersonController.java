/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.controllers;

import java.io.IOException;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.dtos.PersonDto;
import mx.com.praxis.dtos.PersonDto.PersonDtoBuilder;
import mx.com.praxis.services.PersonService;
import mx.com.praxis.utils.Constants;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
@WebServlet("/people")
public class PersonController extends HttpServlet {
	private static final long serialVersionUID = 4231814006439125032L;

	@Inject
	private PersonService personService;

	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		final PersonDtoBuilder personBuilder = PersonDto.builder().firstName(request.getParameter("firstName"))
				.lastName(request.getParameter("lastName"));

		if (request.getParameterMap().containsKey(Constants.ID)) {
			final PersonDto person = personBuilder.id(Long.parseLong(request.getParameter(Constants.ID))).build();

			personService.update(person);
		} else {
			final PersonDto person = personBuilder.build();

			personService.insert(person);
		}

		response.sendRedirect(request.getContextPath() + "/people");
	}

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameterMap().containsKey(Constants.ID)) {
			final long id = Long.parseLong(request.getParameter(Constants.ID));

			request.setAttribute("person", personService.findById(id));
			request.getRequestDispatcher("/people/detail.jsp").forward(request, response);
		} else {
			final int page = Integer.parseInt(Optional.ofNullable(request.getParameter(Constants.PAGE)).orElse("1"));
			final int size = Integer.parseInt(Optional.ofNullable(request.getParameter(Constants.SIZE)).orElse("10"));

			request.setAttribute("people", personService.getPeople(page, size));

			request.getRequestDispatcher("/people/index.jsp").forward(request, response);
		}
	}
}
