<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Students</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap-reboot.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap-grid.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"/>
</head>

<body>
	<h1>Students</h1>
	<a href="${pageContext.request.contextPath}/people">Go to People<i class="fas fa-user fa-2x text-gray-300"></i></a>
	<div class="container">
		<div class="row">
		    <c:forEach var="student" items="${students}">
				<div class="col-xl-3 col-md-6 mb-4">
					<div class="card border-left-primary shadow h-100 py-2">
					  <div class="card-body">
					    <div class="row no-gutters align-items-center">
					      <div class="col mr-2">
					        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">${student.id}</div>
					        <div class="h5 mb-0 font-weight-bold text-gray-800">${student.person.firstName} ${student.person.lastName}</div>
					        <div class="h5 mb-0 font-weight-bold text-gray-800">${student.universityCareer} ${student.yearsBachelorDegree}</div>
					      </div>
					      <div class="col-auto">
					      <a href="${pageContext.request.contextPath}/students?id=${student.id}"><i class="fas fa-id-card fa-2x text-gray-300"></i></a>
					      <a href="${pageContext.request.contextPath}/people?id=${student.person.id}"><i class="fas fa-user fa-2x text-gray-300"></i></a>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
		    </c:forEach>
		</div>
		<div class="row">
			<div class="card shadow mb-4">
				<div class="card-header py-3">
				  <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-plus-circle fa-2x text-gray-300"></i></h6>
				</div>
				<div class="card-body">
					<form action="${pageContext.request.contextPath}/students" method="POST">
					  <div class="form-group">
					    <input type="text" class="form-control form-control-user" id="universityCareer" placeholder="universityCareer" name="universityCareer">
					  </div>
					  <div class="form-group">
					    <input type="text" class="form-control form-control-user" id="yearsBachelorDegree" placeholder="yearsBachelorDegree" name="yearsBachelorDegree">
					  </div>
					  <div class="form-group">
						  <select id="idPerson" name="idPerson">
						  	<c:forEach var="person" items="${people}">
								<option value="${person.id}">${person.firstName} ${person.lastName}</option>
						  	</c:forEach>
						  </select>
					  </div>
					  <input type="submit" value="Send Request">
					</form>	
				</div>
			</div>
		</div>
		<div class="row">
			Praxis 2020
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
</body>

</html>