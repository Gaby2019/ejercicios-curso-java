import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Alumno {
	String nombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String nombreCurso;

	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidoPaterno() {
		return apellidoPaterno;
	}


	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}


	public String getApellidoMaterno() {
		return apellidoMaterno;
	}


	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}


	public String getNombreCurso() {
		return nombreCurso;
	}


	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	
		@Override
		public String toString(){
			return " Nombre: "+ nombre
				+"\n ApellidoPaterno: "+ apellidoPaterno
				+"\n ApellidoMaterno: "+ apellidoMaterno
				+"\n NombreCurso: "+nombreCurso;
		}
		public static void main(String[] args){
			List<Alumno> alumno = new ArrayList<Alumno>();
			for(int i=0;i<20;i++){
				Alumno a =  new Alumno();
				a.setNombre("Alumno"+i+"a");
				a.setApellidoPaterno("ApellidoP"+i);
				a.setApellidoMaterno("ApellidoM"+i);
				a.setNombreCurso("Curso"+i);
				alumno.add(a);
			}
			System.out.println("Todos los elementos ");
			alumno.stream()
			.forEach(System.out::println);
			System.out.println("Longitud lista: "+alumno.stream().count());
			System.out.println("Todos los elementos que terminan con a");
			alumno
			.stream()
			.filter(x -> x.nombre.endsWith("a") )
			.collect(Collectors.toList())
			.forEach(System.out::println);
		}
}
