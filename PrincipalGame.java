import java.util.Optional;

public class PrincipalGame {
	
	public static void main(String[] args){
		String gameName;
		double playerPosx; 
		double playerPosY;
		
		
		playerPosx=6.5;
		playerPosY=5.5;
		//Con lambda más de un parámetro
				Playeable w = (pPosX, pPosY)-> {
					pPosX++;
					pPosY++;
				};
				
				w.walk(playerPosx,playerPosY);

				gameName="hola";
				 
				Gameable g = ()-> {
					System.out.println("gameName:" + gameName);		
				};
				
				g.startGame();

				Soundable s = (song) -> {
					Optional<String> cancion = Optional.of(song);
					cancion.orElse("OtroNombre");
				};

				s.playMusic("Cancion");
	}
}
