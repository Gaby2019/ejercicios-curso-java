
public interface Gameable {
	void startGame();
	
	default void setGameName(String name) {
		System.out.println("Name: " +name);
	}
}
