package com.finaltest.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.finaltest.levels.impl.LogLevels;

public class ThreadTest{
	private static final Logger log = LogManager.getLogger();

	public static void createThreads() {
		
		Runnable r1 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.trace("Test info");
		};
		Thread t1 = new Thread(r1);
		t1.start();
		
		Runnable r2 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Plantas & Zombies");
			log.trace("Test trace");
		};
		Thread t2 = new Thread(r2);
		t2.start();
		
		
		Runnable r3 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Mario Bross");
			log.error("Test error");
		};
		Thread t3 = new Thread(r3);
		t3.start();
		

		Runnable r4 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.debug("Test debug");
		};
		Thread t4 = new Thread(r4);
		t4.start();


		Runnable r5 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.fatal("Test fatal");
		};
		Thread t5 = new Thread(r5);
		t5.start();
		
		Runnable r6 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.warn("Test warn");
		};
		Thread t6 = new Thread(r6);
		t6.start();
		
		Runnable r7 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.log(LogLevels.MUSTFIX, "Test MUSTFIX");
		};
		Thread t7 = new Thread(r7);
		t7.start();
		
		Runnable r8 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.log(LogLevels.DATABASE, "Test DATABASE");
		};
		Thread t8 = new Thread(r8);
		t8.start();
		
		Runnable r9 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.log(LogLevels.FAILOVER, "Test FAILOVER");
		};
		Thread t9 = new Thread(r9);
		t9.start();
		
		Runnable r10= () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.log(LogLevels.FIXLATER, "Test FIXLATER");
		};
		Thread t10 = new Thread(r10);
		t10.start();
		
		Runnable r11 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.log(LogLevels.MYDEBUG, "Test MYDEBUG");
		};
		Thread t11 = new Thread(r11);
		t11.start();
		
		Runnable r12 = () -> {
			ThreadContext.push("name", "Gabriela Hernandez");
			ThreadContext.push("userName", "Pokemon");
			log.warn("Test warn");
		};
		Thread t12 = new Thread(r12);
		t12.start();

	}

}
