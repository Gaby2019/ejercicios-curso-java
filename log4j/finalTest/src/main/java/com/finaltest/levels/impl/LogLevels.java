package com.finaltest.levels.impl;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.finaltest.levels.LogLevelable;
import com.finaltest.thread.ThreadTest;

public class LogLevels implements LogLevelable{
	final Logger log = LogManager.getLogger();
	
	public static Level MUSTFIX = Level.forName("MUSTFIX", 1);
	public static Level DATABASE = Level.forName("DATABASE", 250);
	public static Level FAILOVER = Level.forName("FAILOVER", 350);
	public static Level FIXLATER = Level.forName("FIXLATER", 450);
	public static Level MYDEBUG = Level.forName("MYDEBUG", 550);
	
	
	@Override
	public void createLevels() {
		ThreadTest.createThreads();
	}

	@Override
	public void dummy1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dummy3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dummy2() {
		// TODO Auto-generated method stub
		
	}
	
}
