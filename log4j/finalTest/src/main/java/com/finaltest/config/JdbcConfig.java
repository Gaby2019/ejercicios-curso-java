package com.finaltest.config;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;

public class JdbcConfig {
	private static BasicDataSource dataSource;

	public static Connection conectar() throws SQLException {
		if(dataSource==null) {
			dataSource = new BasicDataSource();
			dataSource.setDriverClassName("com.mysql.jdbc.Driver");
			dataSource.setUrl("jdbc:mysql://localhost:3306/persona?useSSL=false");
			dataSource.setUsername("gaby");
			dataSource.setPassword("12345");
		}
		return dataSource.getConnection();
	}
	
}
