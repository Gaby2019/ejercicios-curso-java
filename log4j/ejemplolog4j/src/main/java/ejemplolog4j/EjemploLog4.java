package ejemplolog4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EjemploLog4 {
	
	private static final Logger log = LogManager.getLogger();

	public static void main(String args[]){
		while(true) {
			log.trace("Log trace");
			log.debug("Log debug");
			log.info("Log info");
			log.warn("Log warn");
			log.error("Log error");
			log.fatal("Log fatal");
		}
		
	}
}
