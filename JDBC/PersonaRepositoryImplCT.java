package persona;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;


public class PersonaRepositoryImplCT implements PersonaRepository{
	    
		private static String URL = "jdbc:sqlserver://172.168.16.128:1443;database=persona;integratedSecurity=true;";
	    
	    String SQL_INSERT="insert into (id, nombre, apellidom, edad, email) values(?,?,?,?,?)";
	    String SQL_UPDATE="update persona set nombre=?";
	    String SQL_DELETE="delete from persona set nombre=?";
	    
	    //Metodo con transacciones para insertar una lista de personas
	    @Override
		public void insertarTodo(List<PersonaBean> personas) throws SQLException{
			try(Connection conexion = DriverManager.getConnection(URL)){
				conexion.setAutoCommit(false);
				try(PreparedStatement preparedStatement = conexion.prepareStatement(SQL_INSERT)){
					
					for(PersonaBean persona: personas) {
						preparedStatement.setLong(1, persona.getId());
						preparedStatement.setString(2, persona.getNombre());
						preparedStatement.setString(3, persona.getApellidom());
						preparedStatement.setShort(3, persona.getEdad());
						preparedStatement.setString(3, persona.getEmail());
					
						preparedStatement.executeUpdate();
					}
					conexion.commit();
				}catch(SQLException ex) {
					conexion.rollback();
					throw ex;
				}
			
			}catch(SQLException e) {
				throw e;
			}
					
		}
		
		//metodo con transacciones para actualizar todo
	    @Override
		public void updateTodo(List<PersonaBean> personas) throws SQLException {
	        try (Connection conexion = DriverManager.getConnection(URL)) {
	        	conexion.setAutoCommit(false);
	            try (PreparedStatement preparedStatement = conexion.prepareStatement(SQL_UPDATE)) {
	                for (PersonaBean persona : personas) {
	                    preparedStatement.setString(1, persona.getNombre());

	                    preparedStatement.executeUpdate();
	                }

	                conexion.commit();
	            } catch (SQLException ex) {
	            	conexion.rollback();
	                throw ex;
	            }

	        } catch (SQLException e) {
	            throw e;
	        }
	    }
		
		//metodo con trassacciones para eliminar todas las personas
	    @Override
	    public void eliminarTodo(List<PersonaBean> personas) throws SQLException {
	        try (Connection conexion = DriverManager.getConnection(URL)) {
	        	conexion.setAutoCommit(false);
	            try (PreparedStatement preparedStatement = conexion.prepareStatement(SQL_DELETE)) {
	                for (PersonaBean persona : personas) {
	                    preparedStatement.setString(1, persona.getNombre());
	                    
	                    preparedStatement.executeUpdate();
	                }

	                conexion.commit();
	            } catch (SQLException ex) {
	            	conexion.rollback();
	                throw ex;
	            }

	        } catch (SQLException e) {
	            throw e;
	        }
	    }

		@Override
		public void insertar(PersonaBean persona) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void actualizar(String nombre, long id) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void eliminar(long id) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public List<PersonaBean> seleccionar() throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}
}
