package persona;
/*
 * Implementacion sin transacciones
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PersonaRepositoryImplST implements PersonaRepository{
	
	private static String URL = "jdbc:sqlserver://172.168.16.128:1443;database=persona;integratedSecurity=true;";
    
    String SQL_INSERT="insert into (nombre, apellidom, edad, email) values(?,?,?,?)";
    String SQL_UPDATE="update persona set nombre=? where id=?";
    String SQL_DELETE="delete from persona where id=?";
    String SQL_SELECT="select *from persona";
    
    private static final int ID = 0;
    private static final String NOMBRE = "nombre";
    private static final String APELIDOM= "apellidom";
    private static final int EDAD = 0;
    private static final String EMAIL = "email";
	    
    	//Metodo insertar sin transacciones
    	@Override
	    public void insertar(PersonaBean persona) throws SQLException {

			try(Connection conexion = DriverManager.getConnection(URL);
					PreparedStatement preparedStatement = conexion.prepareStatement(SQL_INSERT)){
				
				preparedStatement.setString(1, persona.getNombre());
				preparedStatement.setString(2, persona.getApellidom());
				preparedStatement.setShort(3, persona.getEdad());
				preparedStatement.setString(4, persona.getEmail());
				
				preparedStatement.executeUpdate();
				}
			}
	    
    	@Override
	    public void actualizar(String nombre, long id) throws SQLException {

	        try (Connection conexion = DriverManager.getConnection(URL);
	            PreparedStatement preparedStatement = conexion.prepareStatement(SQL_UPDATE)){
	                    preparedStatement.setString(1, nombre);
	                    preparedStatement.setLong(2, id);

	                    preparedStatement.executeUpdate();
	            }
	    }
    	
    	@Override
	    public void eliminar(long id) throws SQLException {
    		try (Connection conexion = DriverManager.getConnection(URL);
    	            PreparedStatement preparedStatement = conexion.prepareStatement(SQL_DELETE)){
    	                    preparedStatement.setLong(1, id);

    	                    preparedStatement.executeUpdate();
    	            }
	    	
	    }
	    
	    @Override
	    public List<PersonaBean> seleccionar() throws SQLException {
	    	List<PersonaBean> personas = new ArrayList<>();
	    	
	    	try(Connection conexion = DriverManager.getConnection(URL);
					PreparedStatement preparedStatement = conexion.prepareStatement(SQL_INSERT)){
	    		
	    		try (ResultSet resultset = preparedStatement.executeQuery()){
	    			
	    			PersonaBean persona;
	    			while(resultset.next()) {
	    				persona=new PersonaBean();
	    				persona.setId(resultset.getLong(ID));
	    				persona.setNombre(resultset.getString(NOMBRE));
	    				persona.setApellidom(resultset.getString(APELIDOM));
	    				persona.setEdad(resultset.getShort(EDAD));
	    				persona.setEmail(resultset.getString(EMAIL));
	    				
	    				personas.add(persona);
	    			}
	    		}
	    	}
	    	return personas;
	    }

		@Override
		public void insertarTodo(List<PersonaBean> personas) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateTodo(List<PersonaBean> personas) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void eliminarTodo(List<PersonaBean> personas) throws SQLException {
			// TODO Auto-generated method stub
			
		}
	    
}
