package persona;

import java.io.Serializable;

public class PersonaBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id=0L;
	private String nombre="";
	private String apellidom="";
	private short edad=0;
	private String email="";
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidom() {
		return apellidom;
	}
	public void setApellidom(String apellidom) {
		this.apellidom = apellidom;
	}
	public short getEdad() {
		return edad;
	}
	public void setEdad(short edad) {
		this.edad = edad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "PersonaBean [id=" + id + ", nombre=" + nombre + ", apellidom=" + apellidom + ", edad=" + edad
				+ ", email=" + email + "]";
	}
	
	/*
	 * Patron de diseño builder
	 * */
	PersonaBean withNombre (String nombre) {
		this.nombre=nombre;
		return this;
	}
	
	PersonaBean withApellidom (String apellidom) {
		this.apellidom=apellidom;
		return this;
	}
	
	PersonaBean withEmail (String email) {
	this.email=email;
	return this;
	}		
}
