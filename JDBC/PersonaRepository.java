package persona;

import java.sql.SQLException;
import java.util.List;

public interface PersonaRepository {
	    
    	//metodos sin transacciones
		public void insertar(PersonaBean persona) throws SQLException;
		public void actualizar(String nombre, long id) throws SQLException;
		public void eliminar(long id) throws SQLException;
		public List<PersonaBean> seleccionar() throws SQLException;
	    
	    //metodos con transacciones
	    public void insertarTodo(List<PersonaBean> personas) throws SQLException;
	    public void updateTodo(List<PersonaBean> personas) throws SQLException;
	    public void eliminarTodo(List<PersonaBean> personas) throws SQLException;

	    static PersonaRepository newInstance(Boolean type){
	        if (type) return new PersonaRepositoryImplCT();
	        return new PersonaRepositoryImplST();
	    }
}
