
public interface Soundable {
	void playMusic(String song);
	
	default void setGameName(String name) {
		System.out.println("Name: " +name);
	}
	
}
